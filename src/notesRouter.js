const express = require('express');

const router = express.Router();
const {
  addNote, getNotes, getNote, updateNote, changeMarkNotedById, deleteNote,
} = require('./notesService');

router.get('/', getNotes);
router.post('/', addNote);
router.get('/:id', getNote);
router.put('/:id', updateNote);
router.patch('/:id', changeMarkNotedById);
router.delete('/:id', deleteNote);

module.exports = {
  notesRouter: router,
};
