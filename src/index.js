const fs = require('fs');
const express = require('express');
const morgan = require('morgan');

const app = express();
const mongoose = require('mongoose');

mongoose.connect('mongodb+srv://root:root@cluster0.kpyatlc.mongodb.net/todoapp?retryWrites=true&w=majority');

const { notesRouter } = require('./notesRouter');
const { usersRouter } = require('./usersRouter');
const { authRouter } = require('./authRouter');
const { authMiddleware } = require('./middleware/authMiddleware');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/notes', authMiddleware, notesRouter);
app.use('/api/users', authMiddleware, usersRouter);
app.use('/api/auth', authRouter);

const start = async () => {
  try {
    app.listen(8080);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();

// ERROR HANDLER

function errorHandler(err, req, res, next) {
  console.error(err);
  res.status(500).send({ message: 'Server error' });
}
app.use(errorHandler);

// const { notesRouter } = require('./notesRouter.js');
// const { usersRouter } = require('./usersRouter.js');
// const { authRouter } = require('./authRouter.js');

// app.use(express.json());
// app.use(morgan('tiny'));

// app.use('/api/notes', notesRouter);
// app.use('/api/users', usersRouter);
// app.use('/api/auth', authRouter);
