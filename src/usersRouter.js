const express = require('express');

const router = express.Router();
const { getUser, deleteUser, changePasswordUser } = require('./usersService');

router.get('/me', getUser);
router.delete('/me', deleteUser);
router.patch('/me', changePasswordUser);

module.exports = {
  usersRouter: router,
};
