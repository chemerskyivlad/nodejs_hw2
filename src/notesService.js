const jwt = require('jsonwebtoken');
const Note = require('./models/Notes');

const addNote = async (req, res) => {
  if (!req.body.text) {
    return res.status(400).send({ message: 'string' });
  }
  const { text } = req.body;
  const note = new Note({
    userId: req.user._id,
    text,
  });

  note.save({}, (err, note) => {
    if (err) {
      return res.status(500).send({ message: 'string' });
    }
    return res.status(200).send({ message: 'Success' });
  });
};
const getNotes = (req, res) => {
  Note.find({ userId: req.user._id }, '-__v', (err, result) => {
    if (err) {
      return res.status(500).send({ message: 'string' });
    } if (result === null) {
      return res.status(500).send({ message: 'string' });
    }
    result.forEach((el, ind, arr) => {
      const {
        _id,
        userId,
        completed,
        text,
        createdAt,
      } = el;
      const createdDate = createdAt;

      arr[ind] = {
        _id, userId, completed, text, createdDate,
      };
    });
    res.json({
      offset: 0,
      limit: 0,
      count: 0,
      notes: result,
    });
  });
};
const getNote = async (req, res) => {
  Note.findOne({ userId: req.user._id, _id: req.params.id }, '-__v', (err, note) => {
    if (err) {
      return res.status(500).send({ message: 'string' });
    } if (note === null) {
      return res.status(500).send({ message: 'string' });
    }
    const {
      _id,
      userId,
      completed,
      text,
      createdAt,
    } = note;
    const createdDate = createdAt;

    noteResult = {
      _id, userId, completed, text, createdDate,
    };
    return res.status(200).send({ note: noteResult });
  });
};
const updateNote = (req, res) => {
  const { text } = req.body;
  Note.findByIdAndUpdate({ userId: req.user._id, _id: req.params.id }, { $set: { text } }, (err, note) => {
    if (err) {
      return res.status(500).send({ message: 'string' });
    } if (note === null) {
      return res.status(500).send({ message: 'string' });
    }
    return res.status(200).send({ message: 'Success' });
  });
};
const changeMarkNotedById = (req, res) => {
  Note.findById({ userId: req.user._id, _id: req.params.id }, (err, note) => {
    if (err) {
      return res.status(500).send({ message: 'string' });
    } if (note === null) {
      return res.status(500).send({ message: 'string' });
    }
    note.completed = !note.completed;
    note.save({}, (err, note) => {
      if (err) {
        return res.status(500).send({ message: 'string' });
      }
      return res.status(200).send({ message: 'Success' });
    });
  });
};
const deleteNote = async (req, res) => {
  Note.findByIdAndDelete({ userId: req.user._id, _id: req.params.id }, (err, note) => {
    if (err) {
      return res.status(500).send({ message: 'string' });
    } if (note === null) {
      return res.status(500).send({ message: 'string' });
    }
    return res.status(200).send({ message: 'Success' });
  });
};

module.exports = {
  addNote,
  getNotes,
  getNote,
  updateNote,
  changeMarkNotedById,
  deleteNote,
};
