const jwt = require('jsonwebtoken');

const authMiddleware = (req, res, next) => {
  const {
    authorization,
  } = req.headers;

  if (!authorization) {
    return res.status(400).send({ message: 'string' });
  }

  const [, token] = authorization.split(' ');

  if (!token) {
    return res.status(400).send({ message: 'string' });
  }

  try {
    const tokenPayload = jwt.verify(token, 'secret-jwt-key');
    req.user = {
      _id: tokenPayload._id,
      username: tokenPayload.username,
      createdDate: tokenPayload.createdDate,
    };
    next();
  } catch (err) {
    return res.status(400).send({ message: 'string' });
  }
};

module.exports = {
  authMiddleware,
};
