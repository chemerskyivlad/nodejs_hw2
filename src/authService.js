const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const User = require('./models/Users');

const registerUser = async (req, res) => {
  const isExist = await User.findOne({ username: req.body.username });
  if (!req.body.username || !req.body.password || isExist) {
    return res.status(400).send({ message: 'string' });
  }

  const { username, password } = req.body;
  const user = new User({
    username,
    password: await bcrypt.hash(password, 10),
  });

  user.save()
    .then(() => {
      res.status(200).send({ message: 'Success' });
    })
    .catch(() => {
      res.status(500).send({ message: 'string' });
    });
};

const loginUser = async (req, res) => {
  const user = await User.findOne({ username: req.body.username });
  if (user && await bcrypt.compare(String(req.body.password), String(user.password))) {
    const payload = { username: user.username, _id: user._id, createdDate: user.createdAt };
    const jwtToken = jwt.sign(payload, 'secret-jwt-key');
    return res.status(200).send({
      message: 'Success',
      jwt_token: jwtToken,
    });
  }
  return res.status(400).send({ message: 'string' });
};

module.exports = {
  registerUser,
  loginUser,
};
